from flake8_artiq.errors import Error

from .tools import evaluate

_simple_statements = ['if True', 'for e in self.some_list', 'while True', 'with some_context']
_complex_statements = ['if self.foo.bar()', 'for _ in foo()', 'while self.baz(5)', 'with some_context.foo()']
_statements = _simple_statements + _complex_statements

_simple_composite_statements = [
    ('if False', 'else'),
    ('if 1 == 2', 'else'),
    ('if 1 == 2', 'elif 2 == 2'),
    ('if 1 == 2', 'elif 1 == 3'),
    ('try', 'except IndexError'),
    ('try', 'finally'),
]
_complex_composite_statements = [
    ('if self.foo(3)', 'else'),
    ('if self.bar(3, 4, 5)', 'elif True'),
]


def test_implicit_sequential():
    source = """
    @kernel
    def foo(self):
        with parallel:
            self.a.on()
            {}:
                self.b.on()
                self.c.on()
    """
    for stmt in _simple_statements:
        errors = {(8, 12, Error.IMPLICIT_SEQUENTIAL)}
        assert evaluate(source.format(stmt)) == errors

    # For complex statements, the extra function call part of the statement but not of its body is implicitly sequential
    for stmt in _complex_statements:
        errors = {
            (7, 12, Error.IMPLICIT_SEQUENTIAL),
            (8, 12, Error.IMPLICIT_SEQUENTIAL),
        }
        assert evaluate(source.format(stmt)) == errors


def test_implicit_sequential_in_expr():
    source = """
    @kernel
    def foo(self):
        with parallel:
            {}
    """
    statements = [  # Added extra spaces to make the error happen in the same column
        '       foo() + foo()',
        '   i = foo() + bar()',
        '  j += foo() + bar()',
        ' while bar() + baz(): pass',
        '    if foo() + bar(): pass',
        ' raise bar() + baz()',
        'return bar() + baz()',
    ]
    for stmt in statements:
        errors = {(5, 23, Error.IMPLICIT_SEQUENTIAL)}
        assert evaluate(source.format(stmt)) == errors


def test_implicit_sequential_composite_statement():
    source = """
    @kernel
    def foo(self):
        with parallel:
            self.a.on()
            {}:
                self.b.on()
                self.c.on()
            {}:
                self.d.on()
                self.e.on()
    """
    errors = {
        (8, 12, Error.IMPLICIT_SEQUENTIAL),
        (10, 12, Error.IMPLICIT_SEQUENTIAL),
        (11, 12, Error.IMPLICIT_SEQUENTIAL),
    }
    for stmt0, stmt1 in _simple_composite_statements:
        assert evaluate(source.format(stmt0, stmt1)) == errors

    # For complex statements, the extra function call part of the statement but not of its body is implicitly sequential
    errors.add((7, 12, Error.IMPLICIT_SEQUENTIAL))
    for stmt0, stmt1 in _complex_composite_statements:
        assert evaluate(source.format(stmt0, stmt1)) == errors


def test_ignored_implicit_sequential():
    source = """
    @kernel
    def foo(self):
        with parallel:
            self.a.on()
            {}:
                self.b.on()
    """
    # Implicit sequential with only one statement does not result in a violation
    for stmt in _simple_statements:
        assert not evaluate(source.format(stmt))
    # For complex statements, the extra function call part of the statement but not of its body is implicitly sequential
    for stmt in _complex_statements:
        assert {(7, 12, Error.IMPLICIT_SEQUENTIAL)} == evaluate(source.format(stmt))


def test_explicit_sequential():
    source = """
    @kernel
    def foo(self):
        with parallel:
            self.a.on()
            {}:
                with sequential:
                    self.b.on()
                    self.c.on()
    """
    for stmt in _statements:
        assert not evaluate(source.format(stmt))


def test_explicit_nested_parallel():
    source = """
    @kernel
    def foo(self):
        with parallel:
            self.a.on()
            {}:
                with parallel:
                    self.b.on()
                    self.c.on()
    """
    for stmt in _statements:
        assert not evaluate(source.format(stmt))


def test_call_in_call():
    source = """
    @kernel
    def foo(self):
        with parallel:
            baz(foo(), bar())
    """
    errors = {
        (5, 12, Error.IMPLICIT_SEQUENTIAL),
        (5, 19, Error.IMPLICIT_SEQUENTIAL),
    }
    assert evaluate(source) == errors
