import flake8_artiq.errors

from .tools import evaluate


def test_empty():
    assert not evaluate('')


def test_error_types():
    for e in flake8_artiq.errors.Error:
        assert isinstance(e.value, tuple)
        assert isinstance(e.code, int)
        assert isinstance(e.msg, str)


def test_unique_error_codes():
    assert len(flake8_artiq.errors.Error) == len({e.code for e in flake8_artiq.errors.Error})


def test_valid_error_codes():
    assert all(0 <= e.code < 1000 for e in flake8_artiq.errors.Error)


def test_unique_error_messages():
    assert len(flake8_artiq.errors.Error) == len({e.msg for e in flake8_artiq.errors.Error})
