from flake8_artiq.errors import Error

from .tools import evaluate


def test_using_assert():
    decorators = ['rpc', 'host_only']
    source = """
    @{}
    def foo(self):
        assert True
    """
    for d in decorators:
        assert not evaluate(source.format(d))


def test_using_assert_in_kernel():
    decorators = ['kernel', 'portable']
    source = """
    @{}
    def foo(self):
        assert True
    """
    for d in decorators:
        assert {(4, 4, Error.USING_ASSERT)} == evaluate(source.format(d))
