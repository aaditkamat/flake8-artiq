from flake8_artiq.errors import Error

from .tools import evaluate, version_mux

_decorators = ['kernel', 'rpc', 'portable']
_types = ['TNone', 'TTuple([TBool, TBool])',
          'TBool', 'TInt32', 'TInt64', 'TFloat',
          'TStr', 'TBytes', 'TByteArray',
          'TList(TInt32)', 'TRange32', 'TRange64',
          'TVar']
_bad_types = ['bool', 'int', 'str', 'np.int32', 'typing.List[int]', 'typing.Tuple[int, int]']


def test_arg_typing():
    source = """
    @{}
    def foo(self, bar: {}):
        pass
    """
    for d in _decorators:
        for t in _types:
            assert not evaluate(source.format(d, t))
        for t in _bad_types:
            assert (version_mux(3, py35=2, py37=2), 0, Error.ARG_TYPING) in evaluate(source.format(d, t))


def test_ret_typing():
    source = """
    @{}
    def foo(self) -> {}:
        pass
    """
    for d in _decorators:
        for t in _types:
            assert not evaluate(source.format(d, t))
        for t in _bad_types:
            assert (version_mux(3, py35=2, py37=2), 0, Error.RETURN_TYPING) in evaluate(source.format(d, t))


def test_typing_comment():
    source = """
    @{}
    def foo(self, bar):  # type: ({}) -> None
        pass
    """
    for d in _decorators:
        for t in _types:
            assert not evaluate(source.format(d, t))
        for t in _bad_types:
            # Type comments are ignored, so no error is raised
            assert not evaluate(source.format(d, t))
