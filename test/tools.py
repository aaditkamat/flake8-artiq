import ast
import textwrap
import typing
import sys

import flake8_artiq.plugin
import flake8_artiq.errors

__all__ = ['evaluate', 'version_mux']

_PY_VERSION = sys.version_info[:2]
"""The current Python version (major, minor)."""


def evaluate(source: str) -> typing.Set[typing.Tuple[int, int, flake8_artiq.errors.Error]]:
    """Dedent and evaluate source, returns a set of error tuples (line, col, :class:`flake8_artiq.errors.Error`)."""
    assert isinstance(source, str)

    # Dedent source and parse
    tree = ast.parse(textwrap.dedent(source))
    # Create the plugin
    plugin = flake8_artiq.plugin.ArtiqPlugin(tree)
    # Return processed error tuples as a set
    return {(line, col, flake8_artiq.errors.msg_to_error(msg)) for line, col, msg, _ in plugin.run()}


_V_T = typing.TypeVar('_V_T')  # Type variable for version type


def version_mux(default: _V_T, *,
                py35: typing.Optional[_V_T] = None,
                py37: typing.Optional[_V_T] = None,
                py38: typing.Optional[_V_T] = None) -> _V_T:
    """Mux that returns a different value based on the current Python version."""
    if py35 is not None and _PY_VERSION == (3, 5):
        return py35
    elif py37 is not None and _PY_VERSION == (3, 7):
        return py37
    elif py38 is not None and _PY_VERSION == (3, 8):
        return py38
    else:
        return default
