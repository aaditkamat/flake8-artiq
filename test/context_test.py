import itertools

from flake8_artiq.errors import Error

from .tools import evaluate

_contexts = ['parallel', 'sequential', 'interleave']


def test_interleave():
    source = """
    @kernel
    def foo(self):
        with interleave:
            pass
    """
    assert (4, 4, Error.USING_INTERLEAVE) in evaluate(source)


def test_timing_context_outside_kernel():
    source = """
    def foo(self):
        with {}:
            pass
    """
    for c in _contexts:
        assert (3, 4, Error.TIME_CX_OUTSIDE_KERNEL) in evaluate(source.format(c))


def test_combined_timing_contexts():
    source = """
    @kernel
    def foo(self):
        with {}, {}:
            pass
    """
    for c0, c1 in itertools.product(_contexts, _contexts):
        assert (4, 4, Error.COMBINED_TIME_CX) in evaluate(source.format(c0, c1))


def test_multiple_contexts():
    source = """
    @kernel
    def foo(self):
        with foo, bar.baz:
            pass
    """
    assert (4, 4, Error.MULTI_ITEM_WITH) in evaluate(source)
