from flake8_artiq.errors import Error

from .tools import evaluate

_functions = ['delay(1.0)', 'delay_mu(300)', 'at_mu(400)', 'now_mu()']


def test_timing_function_outside_kernel():
    source = """
    def foo(self):
        {}
    """
    for fn in _functions:
        assert (3, 4, Error.TIME_FN_OUTSIDE_KERNEL) in evaluate(source.format(fn))


def test_timing_function_in_kernel():
    source = """
    @kernel
    def foo(self):
        {}
    """
    for fn in _functions:
        assert not evaluate(source.format(fn))
