# Flake8 ARTIQ plugin

This repository contains an AST based [flake8](https://flake8.pycqa.org) plugin which performs additional checks
on [ARTIQ](https://github.com/m-labs/ARTIQ) code. Error codes from this plugin have the format `ATQxxx`.
A list of error codes can be found in [this file](flake8_artiq/errors.py).

## Installation

The flake8-artiq plugin requires Python 3.5.3+ and is available on the ARTIQ Nix and Conda channels.

Alternatively, flake8-artiq can be installed using pip.

```shell
$ git clone https://gitlab.com/duke-artiq/flake8-artiq.git
$ cd flake8-artiq/
$ pip3 install ./
```

A successful installation can be verified by printing the flake8 version.

```shell
$ flake8 --version
3.7.9 (flake8-artiq: 0.1.0, mccabe: 0.6.1, pycodestyle: 2.5.0, pyflakes: 2.1.1) CPython 3.7.6 on Linux
```

## Usage

When the flake8-artiq plugin is installed, it is automatically recognized and used by flake8.
Instructions on how to use flake8 can be found on the [flake8 website](https://flake8.pycqa.org).

To only see errors found by the flake8-artiq plugin, use the `--select` option of flake8.

```shell
$ flake8 --select=ATQ
```

## Testing

The code in this repository can be tested by executing the following commands in the root directory of the project.

```shell
$ pytest  # Requires pytest to be installed
$ mypy    # Requires mypy to be installed
$ flake8  # Requires flake8 to be installed
```

## Main contributors

- Leon Riesebos (Duke University)
