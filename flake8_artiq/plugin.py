import ast
import enum
import typing
import sys
import flake8  # type: ignore

from flake8_artiq.errors import Error
from flake8_artiq import __version__ as _plugin_version, __plugin_name__ as _plugin_name

__all__ = ['ArtiqPlugin']

_logger = flake8.LOG.getChild(_plugin_name)
"""The plugin logger."""

# Set the constant types
if sys.version_info[:2] > (3, 5):
    _CONST_TY = (ast.NameConstant, ast.Constant)
else:
    _CONST_TY = (ast.NameConstant,)

# The type of an ID
_ID_T = typing.Any


class NoID:
    """Singleton for no ID."""
    pass


def _get_id(node: ast.AST) -> _ID_T:
    """Recursively resolve the ID of a node.

    :param node: The node to resolve
    :returns: The ID, which is mostly a string but can also be an actual value for constants
    """
    if isinstance(node, ast.Name):
        return node.id
    elif isinstance(node, ast.Attribute):
        return node.attr
    elif isinstance(node, _CONST_TY):
        return _get_id(node.value) if isinstance(node.value, ast.AST) else node.value
    elif isinstance(node, ast.Call):
        return _get_id(node.func)
    elif isinstance(node, ast.withitem):
        return _get_id(node.context_expr)
    elif isinstance(node, ast.Subscript):
        return _get_id(node.value)
    elif isinstance(node, (ast.FunctionDef, ast.ClassDef)):
        return node.name
    else:
        # Emit a warning and return no ID
        _logger.error('Can not get ID of type {}'.format(type(node)))
        return NoID


@enum.unique
class _TimeContextType(enum.Enum):
    """Enumeration for time context types."""

    SEQUENTIAL = 'sequential'
    PARALLEL = 'parallel'
    INTERLEAVE = 'interleave'
    IMPLICIT_SEQUENTIAL = 'implicit_sequential'  # Added when we enter an implicit sequential block

    def __str__(self) -> str:
        v = self.value  # type: str
        return v


_STR_TO_TIME_CONTEXT_TYPE = {str(c): c for c in _TimeContextType if c is not _TimeContextType.IMPLICIT_SEQUENTIAL}
"""Dict to convert strings to a time context type."""

_TIMELESS_GLOBALS = {
    # Value constructors (includes NumPy constructors)
    'bool', 'int', 'float', 'str', 'bytes', 'bytearray', 'list', 'array', 'range', 'int32', 'int64',
    # Exception constructors
    'Exception', 'IndexError', 'ValueError', 'ZeroDivisionError', 'RuntimeError',
    # Build-in Python functions
    'len', 'round', 'abs', 'min', 'max', 'print',
    # ARTIQ utility functions
    'rtio_log', 'core_log',
}  # type: typing.Set[str]
"""Functions in the ARTIQ global namespace that do not affect the timeline."""


class _TimeContext:
    """Data class for a time context."""

    def __init__(self, type_: _TimeContextType):
        assert isinstance(type_, _TimeContextType)

        # Store the type
        self._type = type_
        # The number times this context was entered (stack compression)
        self._num_enter = 1
        # Keep a counter for the number of calls
        self._call_count = 0

    @property
    def type(self) -> _TimeContextType:
        """The type of this context."""
        return self._type

    @property
    def num_entered(self) -> int:
        """The number of times this context was entered."""
        return self._num_enter

    @property
    def num_calls(self) -> int:
        """The number of calls that were registered and could have changed the timeline."""
        return self._call_count

    def enter(self) -> None:
        """Enter this context."""
        self._num_enter += 1

    def exit(self) -> None:
        """Exit this context."""
        self._num_enter -= 1
        assert self._num_enter > 0, 'Context was exited too many times'

    def pop_to_exit(self) -> bool:
        """True if this context needs to be popped of the stack to exit."""
        return self._num_enter == 1

    def register_call(self, fn_id: _ID_T) -> None:
        """Register a call, will filter out calls that are not relevant."""
        if fn_id not in _TIMELESS_GLOBALS:
            self._call_count += 1


class _TimeContextStack:
    """Class for a time context stack."""

    def __init__(self) -> None:
        # Start with a single sequential context
        self._stack = [_TimeContext(_TimeContextType.SEQUENTIAL)]  # type: typing.List[_TimeContext]

    @property
    def top(self) -> _TimeContext:
        """The top of the stack."""
        return self._stack[-1]

    def push(self, context_type: _TimeContextType) -> None:
        """Push a new context to the stack."""
        assert isinstance(context_type, _TimeContextType)

        if context_type is self.top.type:
            # Enter instead of pushing a new context (stack compression)
            self.top.enter()
        else:
            # Push a new context
            self._stack.append(_TimeContext(context_type))

    def pop(self) -> None:
        """Pop the last context off the stack."""

        if self.top.pop_to_exit():
            # Pop the context
            self._stack.pop()
        else:
            # Exit instead of popping the object (stack compression)
            self.top.exit()

    def __len__(self) -> int:
        """The size of the stack."""
        return sum(context.num_entered for context in self._stack)


class _FunctionDef:
    """Data class for a function definition."""

    def __init__(self, node: ast.FunctionDef):
        assert isinstance(node, ast.FunctionDef)

        # Extract a set of decorators
        self._decorators = {_get_id(d) for d in node.decorator_list}

    @property
    def decorators(self) -> typing.Set[_ID_T]:
        """The decorators of this function definition."""
        return self._decorators


_TIME_CX = {'sequential', 'parallel', 'interleave'}  # type: typing.Set[str]
"""ARTIQ time contexts."""
_TIME_FN = {'delay', 'delay_mu', 'at_mu', 'now_mu'}  # type: typing.Set[str]
"""ARTIQ time functions."""

_DECORATORS = {'kernel', 'portable', 'rpc', 'host_only'}  # type: typing.Set[str]
"""ARTIQ decorators."""

_TYPES = {'TNone', 'TTuple',
          'TBool', 'TInt32', 'TInt64', 'TFloat',
          'TStr', 'TBytes', 'TByteArray',
          'TList', 'TRange32', 'TRange64',
          'TVar'}  # type: typing.Set[str]
"""ARTIQ types."""


class Visitor(ast.NodeVisitor):
    """Node visitor class."""

    def __init__(self) -> None:
        # The list that collects all errors found
        self._errors = []  # type: typing.List[typing.Tuple[int, int, Error]]
        # The function definition stack
        self._function_stack = []  # type: typing.List[_FunctionDef]
        # The time context stack
        self._time_stack = _TimeContextStack()

    """Visit functions"""

    def visit_FunctionDef(self, node: ast.FunctionDef) -> None:
        # Extract decorator ID's
        decorators = [_get_id(d) for d in node.decorator_list]
        function_def = _FunctionDef(node)

        # Count the number of ARTIQ decorators
        num_decorators = sum(d in _DECORATORS for d in decorators)

        # Check decorators
        if num_decorators > 1:
            self._add_error(node, Error.MULTIPLE_DECORATORS)

        # Check typing
        if {'kernel', 'portable', 'rpc'} & function_def.decorators:
            types = {_get_id(arg.annotation) for arg in node.args.args if arg.annotation is not None}
            if any(ty not in _TYPES for ty in types):
                self._add_error(node, Error.ARG_TYPING)
            if node.returns is not None and _get_id(node.returns) not in _TYPES:
                self._add_error(node, Error.RETURN_TYPING)

        # Push function definition to the stack
        self._function_stack.append(function_def)
        # Push a sequential time context when entering a function definition
        self._time_stack.push(_TimeContextType.SEQUENTIAL)
        # Continue descend
        self.generic_visit(node)
        # Pop time context from stack
        self._time_stack.pop()
        # Pop function decorators from the stack
        self._function_stack.pop()

    def visit_With(self, node: ast.With) -> None:
        # Extract context ID's
        contexts = [_get_id(i) for i in node.items]
        contexts_set = set(contexts)
        time_contexts = [c for c in contexts if c in _TIME_CX]

        # Check contexts
        if time_contexts and not self._in_kernel():
            self._add_error(node, Error.TIME_CX_OUTSIDE_KERNEL)
        if len(time_contexts) > 1:
            self._add_error(node, Error.COMBINED_TIME_CX)
        if len(contexts) > 1 and self._in_kernel():
            self._add_error(node, Error.MULTI_ITEM_WITH)
        if 'interleave' in contexts_set:
            self._add_error(node, Error.USING_INTERLEAVE)

        if time_contexts:
            # Push the last time context
            self._time_stack.push(_STR_TO_TIME_CONTEXT_TYPE[time_contexts[-1]])
        else:
            # Push an implicit sequential time context
            self._push_implicit_sequential()

        # Continue descend
        self.generic_visit(node)
        # Pop time context from stack
        self._time_stack.pop()

    def visit_Call(self, node: ast.Call) -> None:
        # Get function id
        func = _get_id(node)

        # Check calls
        if func in _TIME_FN and not self._in_kernel():
            self._add_error(node, Error.TIME_FN_OUTSIDE_KERNEL)

        # Register this call in the time stack
        self._time_stack.top.register_call(func)
        # Check for implicit sequential timing
        if self._time_stack.top.type is _TimeContextType.IMPLICIT_SEQUENTIAL and self._time_stack.top.num_calls > 1:
            self._add_error(node, Error.IMPLICIT_SEQUENTIAL)

        # Continue descend
        self._implicit_sequential_visit(node)

    def visit_Assert(self, node: ast.Assert) -> None:
        # Check for usage of assert in kernels
        if self._in_kernel() or self._in_portable():
            self._add_error(node, Error.USING_ASSERT)

        # Continue descend
        self._implicit_sequential_visit(node)

    def visit_Return(self, node: ast.Return) -> None:
        self._implicit_sequential_visit(node)

    def visit_Assign(self, node: ast.Assign) -> None:
        self._implicit_sequential_visit(node)

    def visit_AugAssign(self, node: ast.AugAssign) -> None:
        self._implicit_sequential_visit(node)

    def visit_For(self, node: ast.For) -> None:
        self._implicit_sequential_visit(node)

    def visit_While(self, node: ast.While) -> None:
        self._implicit_sequential_visit(node)

    def visit_If(self, node: ast.If) -> None:
        self._implicit_sequential_visit(node)

    def visit_Try(self, node: ast.Try) -> None:
        self._implicit_sequential_visit(node)

    def visit_Raise(self, node: ast.Raise) -> None:
        self._implicit_sequential_visit(node)

    def visit_Expr(self, node: ast.Expr) -> None:
        self._implicit_sequential_visit(node)

    """Helper functions"""

    def _add_error(self, node: typing.Union[ast.stmt, ast.expr], error: Error) -> None:
        """Add an error to the error list."""
        self._errors.append((node.lineno, node.col_offset, error))

    def _push_implicit_sequential(self) -> None:
        """Add an implicit sequential time context as a result of entering a statement."""
        if self._time_stack.top.type in {_TimeContextType.IMPLICIT_SEQUENTIAL, _TimeContextType.PARALLEL}:
            # Push another implicit sequential time context
            self._time_stack.push(_TimeContextType.IMPLICIT_SEQUENTIAL)
        else:
            # Push a regular sequential time context
            self._time_stack.push(_TimeContextType.SEQUENTIAL)

    def _implicit_sequential_visit(self, node: ast.AST) -> None:
        # Push an implicit sequential time context
        self._push_implicit_sequential()
        # Continue descend
        self.generic_visit(node)
        # Pop time context from stack
        self._time_stack.pop()

    def _in_kernel(self) -> bool:
        """True if the current node is inside a kernel function."""
        if self._function_stack:
            return 'kernel' in self._function_stack[-1].decorators
        else:
            return False  # Not in a function def

    def _in_portable(self) -> bool:
        """True if the current node is inside a portable function."""
        if self._function_stack:
            return 'portable' in self._function_stack[-1].decorators
        else:
            return False  # Not in a function def

    def _in_rpc(self, implicit: bool = False) -> bool:
        """True if the current node is inside an RPC function.

        :param implicit: True to also include implicit RPC functions (i.e. functions without ARTIQ decorators)
        """
        if self._function_stack:
            explicit_rpc = 'rpc' in self._function_stack[-1].decorators
            if implicit:
                return explicit_rpc or not self._function_stack[-1].decorators & _DECORATORS
            else:
                return explicit_rpc
        else:
            return False  # Not in a function def

    def _in_host_only(self) -> bool:
        """True if the current node is inside a host only function."""
        if self._function_stack:
            return 'host_only' in self._function_stack[-1].decorators
        else:
            return False  # Not in a function def

    def errors(self) -> typing.Iterator[typing.Tuple[int, int, Error]]:
        """Returns an iterator of tuples (line, col, Error)."""
        return iter(self._errors)


class ArtiqPlugin:
    """The flake8 ARTIQ plugin class."""

    name = _plugin_name  # type: str
    """The name of this plugin as shown in flake8."""
    version = _plugin_version  # type: str
    """The version of this plugin as shown in flake8."""

    _ERR_FORMAT = 'ATQ{code:03d} {msg}'  # type: str
    """Format of error messages."""

    def __init__(self, tree: ast.AST):
        # The abstract syntax tree
        self._tree = tree

    def run(self) -> typing.Generator[typing.Tuple[int, int, str, typing.Type[typing.Any]], None, None]:
        """The run function, called by flake8."""
        # Visit the tree
        visitor = Visitor()
        visitor.visit(self._tree)

        # Yield errors
        for line, col, error in visitor.errors():
            yield line, col, self._ERR_FORMAT.format(code=error.code, msg=error.msg), self.__class__
