__plugin_name__ = 'flake8-artiq'  # type: str
"""The (unique) name of this flake8 plugin."""

__version__ = '0.1.0'  # type: str
"""The version of this plugin."""
