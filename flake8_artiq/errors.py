import enum
import typing

__all__ = ['Error', 'msg_to_error']


@enum.unique
class Error(enum.Enum):
    # ARTIQ timing constructs
    TIME_FN_OUTSIDE_KERNEL = (101, 'timing function used outside of kernel')
    TIME_CX_OUTSIDE_KERNEL = (102, 'timing context used outside of kernel')
    COMBINED_TIME_CX = (103, 'more than one timing context in a single `with` statement')

    # ARTIQ decorators
    MULTIPLE_DECORATORS = (201, 'more than one ARTIQ decorator for one function')

    # ARTIQ typing
    ARG_TYPING = (301, 'used non-ARTIQ typing for argument')
    RETURN_TYPING = (302, 'used non-ARTIQ typing for return value')

    # Others
    MULTI_ITEM_WITH = (901, '`with` statement with multiple items (see artiq#1478)')
    USING_INTERLEAVE = (902, 'using `interleave`, which has limited documentation')
    IMPLICIT_SEQUENTIAL = (903, 'implicit sequential timing in shallow parallel block (see artiq#1555)')
    USING_ASSERT = (904, 'using `assert` in a kernel is not supported')

    @property
    def code(self) -> int:
        """The error code."""
        code = self.value[0]  # type: int
        return code

    @property
    def msg(self) -> str:
        """The error message."""
        msg = self.value[1]  # type: str
        return msg


_CODE_TO_ERROR = {e.code: e for e in Error}  # type: typing.Dict[int, Error]
"""Dict to convert error codes to error enums."""


def msg_to_error(msg: str) -> Error:
    """Convert an error message back to an :class:`Error`."""
    code = int(msg[3:7])
    return _CODE_TO_ERROR[code]
